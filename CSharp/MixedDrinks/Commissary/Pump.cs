﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace MixedDrinks.Commissary
{
  public class Pump
  {
    private readonly int number;

    internal Pump(int number, Vat[] sources, Vat[] destinations)
    {
      Sources = sources;
      Destinations = destinations;
      this.number = number;
    }

    public Vat[] Sources { get; }
    public Vat[] Destinations { get; }
    public Vat Destination { get; private set; }
    public Vat Source { get; private set; }

    public void SwitchOutputToDrain()
    {
      Console.WriteLine("Switching " + this + " output to drain.");
    }

    public void SwitchInputToWaterLine()
    {
      Console.WriteLine("Switching" + this + " input to water line.");
    }

    public override string ToString()
    {
      return "pump #" + number;
    }

    public void Transfer(int gallons)
    {
      Console.WriteLine("Transferring " + gallons + " through " + this + ".");
    }

    public void SwitchDestinationToVat(Vat destination)
    {
      Console.WriteLine("Switching " + this + " output to " + destination);
      Destination = destination;
    }

    public void SwitchSourceToVat(Vat source)
    {
      Console.WriteLine("Switching " + this + " input to " + source);
      Source = source;
    }
  }
}