﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;

namespace MixedDrinks.Commissary
{
  public class ResourceManager
  {
    private readonly Queue<Pump> freePumps = new Queue<Pump>();
    private readonly Queue<Vat> freeVats = new Queue<Vat>();

    public ResourceManager(int destinationVats, int pumps, params string[] sourceContents)
    {
      var vatNumber = 0;
      var sourceList = new List<Vat>();
      foreach (var sourceContent in sourceContents)
        sourceList.Add(new Vat(vatNumber++, sourceContent));
      var destinationList = new List<Vat>();
      for (var i = 0; i < destinationVats; ++i)
        destinationList.Add(new Vat(vatNumber++, "mixed"));
      foreach (var vat in destinationList)
        freeVats.Enqueue(vat);

      var sources = sourceList.ToArray();
      var destinations = destinationList.ToArray();
      for (var i = 0; i < pumps; ++i)
        freePumps.Enqueue(new Pump(i, sources, destinations));
    }

    public Lock<Pump> GetPump()
    {
      return new Lock<Pump>(freePumps.Dequeue(), freePumps.Enqueue);
    }

    public Lock<Vat> GetDestinationVat()
    {
      return new Lock<Vat>(freeVats.Dequeue(), freeVats.Enqueue);
    }
  }
}