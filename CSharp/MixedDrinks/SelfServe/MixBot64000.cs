﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Threading;

namespace MixedDrinks.SelfServe
{
  public class MixBot64000
  {
    public enum Foam
    {
      WhippedCream,
      SteamedMilk
    }

    public enum Granules
    {
      Salt,
      Sugar
    }

    public enum Liquid
    {
      Water,
      ClubSoda,
      OrangeJuice,
      CranberryJuice,
      PineappleJuice,
      StraberryJuice,
      RaspberryJuice,
      LimeJuice,
      LemonJuice,
      Lemonade,
      IcedTea,
      SparklingWine,
      Vokda,
      Gin,
      Whiskey,
      Tequila,
      BlendedScotchWhiskey,
      IrishWhiskey,
      SweetVermouth,
      DryVermouth,
      Bitters,
      IrishCream,
      Brandy,
      CognacBrandy,
      GrenadineSyrup,
      GingerAle,
      LemonLimeSoda,
      ColaSoda,
      TripleSec,
      SimpleSyrup
    }

    public enum Solid
    {
      Olives,
      LemonSlices,
      LimeSlices,
      OrangeSlice,
      MintLeaves,
      Cherries,
      Blackberries,
      Strawberries
    }

    public enum Glass
    {
      Highball,
      Cocktail,
      Martini,
      Champagne,
      Pint,
    }

    public MixBot64000(int glasses, Liquid[] wells, Solid[] caddies, Foam[] nozzles, Granules[] silos)
    {
      Glasses = glasses;
      Wells = wells;
      Caddies = caddies;
      Nozzles = nozzles;
      Silos = silos;
    }

    public int Glasses { get; }
    public Liquid[] Wells { get; }
    public Solid[] Caddies { get; }
    public Foam[] Nozzles { get; }
    public Granules[] Silos { get; }

    public void DispenseIce(float ounces)
    {
      DoTimedAction("Switching to ice cubes", 50);
    }

    public void StrainGlassToGlass(int sourceGlass, int targetGlass)
    {
      DoTimedAction("Straining glass #" + sourceGlass + " into " + targetGlass, 1000);
    }

    public void SwitchToWell(int glass, int number)
    {
      SwitchToDispenser("well", glass, number, Wells);
    }

    public void SwitchToCaddy(int glass, int number)
    {
      SwitchToDispenser("caddy", glass, number, Caddies);
    }

    public void SwitchToNozzle(int glass, int number)
    {
      SwitchToDispenser("nozzle", glass, number, Nozzles);
    }

    public void SwitchToSilo(int glass, int number)
    {
      SwitchToDispenser("silo", glass, number, Silos);
    }

    public void DispenseOunces(float ounces)
    {
      DoTimedAction("Dispensing " + ounces + " oz", (int) (ounces * 100));
    }

    public void DispenseCount(int count)
    {
      DoTimedAction("Dispensing " + count, 50 + count * 10);
    }

    public void ServeGlass(int number)
    {
      DoTimedAction("Serving glass #" + number, 100);
    }

    public void SetGlass(int number, Glass glass)
    {
      DoTimedAction("Switching glass #" + number + " to " + glass, 250);
    }

    private static void DoTimedAction(string action, int time)
    {
      Console.Write(action + "...");
      Thread.Sleep(time);
      Console.WriteLine("done.");
    }

    private static void SwitchToDispenser<T>(string type, int glass, int number, T[] items)
    {
      DoTimedAction("Switching glass # " + glass + " to " + type + " #" + number + " (" + items[number] + ")", 250);
    }
  }
}