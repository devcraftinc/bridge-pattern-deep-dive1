﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace MixedDrinks.BartenderFrontEnd
{
  public class ModalInteraction
  {
    public enum YesNo
    {
      No,
      Yes
    }

    public YesNo ShowYesNo(string message)
    {
      Console.WriteLine(message + " (Y/N)");

      if (WaitForKey(ConsoleKey.Y, ConsoleKey.N) == ConsoleKey.Y)
        return YesNo.Yes;

      return YesNo.No;
    }

    public void Do(string command)
    {
      Console.WriteLine(command + " (press space when done)");
      WaitForKey(ConsoleKey.Spacebar);
    }

    private ConsoleKey WaitForKey(params ConsoleKey[] keys)
    {
      do
      {
        var info = Console.ReadKey();
        foreach (var key in keys)
          if (info.Key == key)
            return info.Key;
      } while (true);
    }
  }
}