// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package sessions.Commissary;

import java.util.*;
import java.util.concurrent.*;

public class ResourceManager {
    private final Queue<Pump> freePumps = new LinkedBlockingQueue<>();
    private final Queue<Vat> freeVats = new LinkedBlockingQueue<>();

    public ResourceManager(int destinationVats, int pumps, String... sourceContents) {
        int vatNumber = 0;
        ArrayList<Vat> sourceList = new ArrayList<Vat>();
        for (String sourceContent : sourceContents)
            sourceList.add(new Vat(vatNumber++, sourceContent));
        ArrayList<Vat> destinationList = new ArrayList<Vat>();
        for (int i = 0; i < destinationVats; ++i)
            destinationList.add(new Vat(vatNumber++, "mixed"));
        for (Vat vat : destinationList)
            freeVats.add(vat);

        Vat[] sources = sourceList.toArray(new Vat[0]);
        Vat[] destinations = destinationList.toArray(new Vat[0]);
        for (int i = 0; i < pumps; ++i)
            freePumps.add(new Pump(i, sources, destinations));
    }

    public Lock<Pump> getPump() {
        final Pump pump = freePumps.remove();
        return new Lock<>(pump, () -> freePumps.add(pump));
    }

    public Lock<Vat> getDestinationVat() {
        final Vat vat = freeVats.remove();
        return new Lock<>(vat, () -> freeVats.add(vat));
    }
}

