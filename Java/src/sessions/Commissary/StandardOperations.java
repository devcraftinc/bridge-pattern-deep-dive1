// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package sessions.Commissary;

public class StandardOperations {
    public static void flush(Pump pump) {
        pump.SwitchInputToWaterLine();
        pump.SwitchOutputToDrain();
        pump.Transfer(100);
    }

    public static void clean(Pump pump, Vat toClean) {
        toClean.drain();
        pump.SwitchDestinationToVat(toClean);
        pump.SwitchInputToWaterLine();
        pump.Transfer(1000000);
        toClean.stir(1);
        toClean.drain();
    }

    public static void transferContent(Pump pump, Vat source, Vat dest, int gallons) {
        pump.SwitchSourceToVat(source);
        pump.SwitchDestinationToVat(dest);
        pump.Transfer(gallons);
    }
}

