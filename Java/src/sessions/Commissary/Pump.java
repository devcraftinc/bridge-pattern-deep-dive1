// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package sessions.Commissary;

public class Pump {
    private final int number;

    Pump(int number, Vat[] sources, Vat[] destinations) {
        this.sources = sources;
        this.destinations = destinations;
        this.number = number;
    }

    private Vat[] sources;

    public Vat[] getSources() {
        return sources;
    }

    private Vat[] destinations;

    public Vat[] getDestinations() {
        return destinations;
    }

    private Vat destination;
    private Vat source;

    public Vat getDestination() {
        return destination;
    }

    public Vat getSource() {
        return source;
    }

    public void SwitchOutputToDrain() {
        System.out.println("Switching " + this + " output to drain.");
    }

    public void SwitchInputToWaterLine() {
        System.out.println("Switching" + this + " input to water line.");
    }

    @Override
    public String toString() {
        return "pump #" + number;
    }

    public void Transfer(int gallons) {
        System.out.println("Transferring " + gallons + " through " + this + ".");
    }

    public void SwitchDestinationToVat(Vat destination) {
        System.out.println("Switching " + this + " output to " + destination);
        this.destination = destination;
    }

    public void SwitchSourceToVat(Vat source) {
        System.out.println("Switching " + this + " input to " + source);
        this.source = source;
    }
}

