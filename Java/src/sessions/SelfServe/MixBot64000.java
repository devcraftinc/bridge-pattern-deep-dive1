// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package sessions.SelfServe;

public class MixBot64000 {
    public enum Foam {
        WhippedCream,
        SteamedMilk
    }

    public enum Granules {
        Salt,
        Sugar
    }

    public enum Liquid {
        Water,
        ClubSoda,
        OrangeJuice,
        CranberryJuice,
        PineappleJuice,
        StraberryJuice,
        RaspberryJuice,
        LimeJuice,
        LemonJuice,
        Lemonade,
        IcedTea,
        SparklingWine,
        Vokda,
        Gin,
        Whiskey,
        Tequila,
        BlendedScotchWhiskey,
        IrishWhiskey,
        SweetVermouth,
        DryVermouth,
        Bitters,
        IrishCream,
        Brandy,
        CognacBrandy,
        GrenadineSyrup,
        GingerAle,
        LemonLimeSoda,
        ColaSoda,
        TripleSec,
        SimpleSyrup
    }

    public enum Solid {
        Olives,
        LemonSlices,
        LimeSlices,
        OrangeSlice,
        MintLeaves,
        Cherries,
        Blackberries,
        Strawberries
    }

    public enum Glass {
        Highball,
        Cocktail,
        Martini,
        Champagne,
        Pint,
    }

    public MixBot64000(int glasses, Liquid[] wells, Solid[] caddies, Foam[] nozzles, Granules[] silos) {
        this.glasses = glasses;
        this.wells = wells;
        this.caddies = caddies;
        this.nozzles = nozzles;
        this.silos = silos;
    }

    private int glasses;

    public int getGlasses() {
        return glasses;
    }

    private Liquid[] wells;

    public Liquid[] getWells() {
        return wells;
    }

    private Solid[] caddies;

    public Solid[] getCaddies() {
        return caddies;
    }

    private Foam[] nozzles;

    public Foam[] getNozzles() {
        return nozzles;
    }

    private Granules[] silos;

    public Granules[] getSilos() {
        return silos;
    }

    public void dispenseIce(float ounces) {
        doTimedAction("Switching to ice cubes", 50);
    }

    public void strainGlassToGlass(int sourceGlass, int targetGlass) {
        doTimedAction("Straining glass #" + sourceGlass + " into " + targetGlass, 1000);
    }

    public void switchToWell(int glass, int number) {
        switchToDispenser("well", glass, number, wells);
    }

    public void switchToCaddy(int glass, int number) {
        switchToDispenser("caddy", glass, number, caddies);
    }

    public void switchToNozzle(int glass, int number) {
        switchToDispenser("nozzle", glass, number, nozzles);
    }

    public void switchToSilo(int glass, int number) {
        switchToDispenser("silo", glass, number, silos);
    }

    public void dispenseOunces(float ounces) {
        doTimedAction("Dispensing " + ounces + " oz", (int) (ounces * 100));
    }

    public void dispenseCount(int count) {
        doTimedAction("Dispensing " + count, 50 + count * 10);
    }

    public void setGlass(int number, Glass glass) {
        doTimedAction("Switching glass #" + number + " to " + glass, 250);
    }

    public void serveGlass(int number) {
        doTimedAction("Serving glass #" + number, 100);
    }

    private static void doTimedAction(String action, int time) {
        System.out.print(action + "...");
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("done.");
    }

    private static <T> void switchToDispenser(String type, int glass, int number, T[] items) {
        doTimedAction("Switching glass # " + glass + " to " + type + " #" + number + " (" + items[number] + ")", 250);
    }
}

